import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {ApiService} from '../../_services/api/api.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  public form: FormGroup;
  public wrongCredentials: boolean = false;
  errors: Array<any> = [];

  constructor(private fb: FormBuilder, private router: Router,public api: ApiService) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )] , password: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }

  onSubmit() {

      this.errors = [];
      let payload: any = {};
      payload.username = this.form.value.uname;
      payload.password = this.form.value.password;
      console.log(payload);

      this.api.sendPost('WebCustomerAdminLogin', payload).subscribe(data => {
          if (data.length == 0) {
              this.wrongCredentials = true;
          }
          else {
            //admin
            if (data[0].isAdmin == 1)
            {
                this.wrongCredentials = false;
                localStorage.setItem('user_id', data[0].id);
                localStorage.setItem('user_type', data[0].isAdmin);
                this.router.navigate(['/','customers','index']);
                //this.router.navigate ( [ '/' ] );
            }

          }
      }, error => {
          this.errors.push({message: "שגיאה בהתבחרות יש לנסות שוב"});
      });
  }

}
