import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    deleteModal: any;
    companyToDelete: any;
    public folderName:string = 'users';
    public addButton:string = 'הוסף משתמש'
    customer_id: any;


    constructor(public MainService: MainService, private modalService: NgbModal ,settings: SettingsService , private route: ActivatedRoute, public router:Router) {
        this.route.queryParams.subscribe(params => {
            this.customer_id = params['customer_id'];
            console.log("11 : " , this.customer_id);
            this.host = settings.host,
            this.avatar = settings.avatar
            this.getItems();
        });



    }

    ngOnInit() {
    }

    getItems() {


        this.MainService.GetCategories('GetUsers',this.customer_id).then((data: any) => {
            console.log("GetUsers : ", data) ,
            this.ItemsArray = data,
            this.ItemsArray1 = data

        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('DeleteUser', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }

    withdrawHistoryPage(user_id) {
        this.router.navigate(['/','withdrawal_history','index'], { queryParams: { id:user_id,customer_id: this.customer_id } });
    }

    addUserPage()
    {
        this.router.navigate(['/','users','add'], { queryParams: { customer_id: this.customer_id } });
    }

    editUserPage(index)
    {
        this.router.navigate(['/','users','edit'], { queryParams: { customer_id: this.customer_id,id: index } });
    }

    goRecommededPage(id) {
        this.router.navigate(['/','recommended','index'], { queryParams: { customer_id: this.customer_id,id: id } });

    }


    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.phone.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
