import {Component, OnInit, ElementRef, ViewChild, NgZone, ChangeDetectionStrategy} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup,FormControl, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {DomSanitizer} from '@angular/platform-browser';
import * as Quill from 'quill';
import {ApiService} from '../../_services/api/api.service';



@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './settings.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './settings.component.css']
})

export class SettingsComponent  implements OnInit {
    public Id;
    public navigateTo:string = '/customers/index';
    public Items:any[]=[];
    public rowsNames:any[] = ['דף ראשי וידאו','המלץ לחבר וידאו','פרטים אישיים וידאו','שאלות ותשובות וידאו','החשבון שלי וידאו','מי אנחנו וידאו','כותרת מי אנחנו','טקסט דינאמי שישלח לווידוי טלפון ב SMS'];
    public rows:any[] = ['home_video','recommend_friend_video','personal_details_video','faq_video','my_account_video','about_us_video','about_title','verify_phone_desc'];
    public Item;
    public host;
    public imagepath;
    public Change:boolean = false;
    logo = {path: null, file: null};
    errors: Array<any> = [];

    form: FormGroup = this.fb.group({
        home_video: new FormControl('', Validators.required),
        recommend_friend_video: new FormControl('', Validators.required),
        personal_details_video: new FormControl('', Validators.required),
        faq_video: new FormControl('', Validators.required),
        my_account_video: new FormControl('', Validators.required),
        about_us_video: new FormControl('', Validators.required),
        about_title: new FormControl('', Validators.required),
        about_description: new FormControl('', Validators.required),
        verify_phone_desc: new FormControl(''),
    });


    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService,public fb: FormBuilder,private sanitizer: DomSanitizer,public api: ApiService) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            this.imagepath = settings.imagepath;
            console.log("fkitchens " ,this.Item );
        });
    }

    async onSubmit()
    {
        this.errors = [];

        let payload: any = {};
        payload.id = this.Item.id;
        payload.home_video = this.form.value.home_video;
        payload.recommend_friend_video = this.form.value.recommend_friend_video;
        payload.personal_details_video = this.form.value.personal_details_video;
        payload.faq_video = this.form.value.faq_video;
        payload.my_account_video = this.form.value.my_account_video;
        payload.about_us_video = this.form.value.about_us_video;
        payload.about_title = this.form.value.about_title;
        payload.about_description = this.form.value.about_description;
        payload.verify_phone_desc = this.form.value.verify_phone_desc;



        console.log(payload);
        this.api.sendPost('webUpdateCustomerSettings', payload).subscribe(data => {
            this.router.navigate([this.navigateTo]);
        }, error => {
            this.errors.push({message: "שגיאה בעידכון יש לנסות שוב"});
        });

    }

    ngOnInit() {


        this.form.setValue({
            home_video: this.Item.home_video,
            recommend_friend_video: this.Item.recommend_friend_video,
            personal_details_video: this.Item.personal_details_video,
            faq_video: this.Item.faq_video,
            my_account_video: this.Item.my_account_video,
            about_us_video: this.Item.about_us_video,
            about_title: this.Item.about_title,
            about_description: this.Item.about_description,
            verify_phone_desc: this.Item.verify_phone_desc,
        });



        const quill = new Quill('#editor-container', {
            modules: {toolbar: {container: '#toolbar-toolbar'}},
            theme: 'snow'
        });

        quill.on('text-change', () => {
            this.form.controls.about_description.setValue(quill.root.innerHTML);
        });



        this.logo.path = this.imagepath + this.Item.image;
        quill.clipboard.dangerouslyPasteHTML(this.Item.about_description);
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }
}
