import {Routes} from '@angular/router';


import {EditComponent} from "./edit/edit.component";
import {AddComponent} from "./add/add.component";
import {IndexComponent} from "./index/index.component";
import {SettingsComponent} from "./settings/settings.component";

export const MaindRoutes: Routes = [{
    path: '',
    children: [{
        path: 'index',
        component: IndexComponent,
        data: {
            heading: 'לקוחות'
        }
    },{
        path: 'edit',
        component: EditComponent,
        data: {
            heading: 'עריכת לקוח'
        }
    },{
        path: 'add',
        component: AddComponent,
        data: {
            heading: 'הוספת לקוח'
        }
    },{
        path: 'customer_settings',
        component: SettingsComponent,
        data: {
            heading: 'הגדרות'
        }
    }]
}];
