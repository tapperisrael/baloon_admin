import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {SettingsService} from "../../../settings/settings.service";

@Injectable()
export class ApiService {


    constructor(public http: Http,public settings: SettingsService) {}

    public sendGet (url: string,): Observable<any> {

        return this.http.get(this.settings.ServerUrl + url).map(r => {
            console.log(r.json());
            return r.json();
        });

    }

    public sendPost (url: string, data: any): Observable<any> {

        let body = new FormData();
        for (let key of Object.keys(data)){
            body.append(key, data[key]);
        }

        return this.http.post(this.settings.ServerUrl + url, body).map(r => {
            console.log(r.json());
            return r.json();
        });

    }
}
