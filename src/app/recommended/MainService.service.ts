
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {SettingsService} from "../../settings/settings.service";



//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class MainService
{
    public ServerUrl = "";
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Companies:any[]=[];
    public CompanyArray:any[]=[];
    public Kitchens:any[]=[];
    public Items:any[]=[];

    constructor(private http:Http ,  settings:SettingsService)
    {
        this.ServerUrl = settings.ServerUrl;
    };

    GetAllItems(url:string,customer_id,id)
    {
        let body = new FormData();
        body.append('customer_id',customer_id);
        body.append('id',id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Items = data }).toPromise();
    }


    SaveLead(url:string,id,lead_status,lead_remarks)
    {
        let body = new FormData();
        body.append('id',id);
        body.append('lead_status',lead_status);
        body.append('lead_remarks',lead_remarks);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Companies = data }).toPromise();
    }

    GetItemById(url:string,id)
    {
        let body = new FormData();
        body.append('id',id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Companies = data }).toPromise();
    }

    EditItem(url,Item,File)
    {
        console.log("IT1 : " , File)
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    AddItem(url,Item,File)
    {
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        return this.http.post (this.ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
    }

    DeleteItem(url,Id)
    {
        let body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }

    closeDeal(url,customer_id,Deal_Id,user_id)
    {
        let body = 'customer_id=' + customer_id+'&deal_id=' + Deal_Id+'&user_id=' + user_id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }



    // AddCompany(url,Company)
    // {
    //     this.CompanyArray = Company;
    //     let body = 'company=' + JSON.stringify(this.CompanyArray);
    //     return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    // }
    //
    // EditCompany(url,Company)
    // {
    //     this.CompanyArray = Company;
    //     let body = 'company=' + JSON.stringify(this.CompanyArray);
    //     return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    // }
    //
    // DeleteCompany(url,Id)
    // {
    //     let body = 'id=' + Id;
    //     return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    // }
};


