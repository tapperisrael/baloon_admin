import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'users';
    public addButton:string = 'הוסף משתמש'
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    Id:any;
    public customer_id;

    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute, public router:Router) {
        this.route.queryParams.subscribe(params => {
            this.customer_id = params['customer_id'];
            this.Id = params['id'];
            console.log("11 : " , this.Id)
            this.host = settings.host;
            this.avatar = settings.avatar;
            this.getItems();
        });
    }

    ngOnInit() {
    }

    getItems()
    {
        this.MainService.GetAllItems('GetRecommendedById', this.customer_id,this.Id ).then((data: any) => {
            console.log("GetCategories12 : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }


    goBackPage()
    {
        this.router.navigate(['/','users','index'], { queryParams: { customer_id: this.customer_id } });
    }

    closeDeal(deal_id,close_deal)
    {
        if (close_deal == 0)
        {
            this.MainService.closeDeal('CloseRecommendedDeal',this.customer_id,deal_id,this.Id).then((data: any) => {
                this.ItemsArray = data , console.log("CloseRecommendedDeal : ", data);
            })
        }

    }

    changeRemarks(event,row) {
        row.lead_remarks = event.target.value;
    }

    changeStatus(event,row) {
        row.lead_status = event.target.value;
    }

    saveDetails(row) {
        this.MainService.SaveLead('WebSaveRecommendedLead', row.id,row.lead_status,row.lead_remarks).then((data: any) => {
            alert ("עודכן בהצלחה");
        })
    }


    DeleteItem() {
        this.MainService.DeleteItem('DeleteRecommend', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
